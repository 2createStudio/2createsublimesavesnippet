# 2createSublimeSaveSnippet

## Installation

1. Add Repository https://bitbucket.org/2createStudio/2createsublimesavesnippet.git (ctrl+shift+p -> Package Control: Add Repository)
1. Install 2createSublimeSaveSnippet package via PackageControl (ctrl+shift+p -> Package Control: Install Package).

## Commands

- ctrl+alt+x (super+alt+x) - Save emmet snippet
- ctrl+alt+s (super+alt+s) - Save regular sublime snippet

