import sublime, sublime_plugin
import os
import json

class SaveSnippetCommand(sublime_plugin.TextCommand, sublime_plugin.WindowCommand):
	def run(self, edit, type):
		view = self.view
		regions = view.sel()
		packagesPath = sublime.packages_path()
		pathToFile = packagesPath + '/2createEmmet/extensions/snippets-saved.json'
		pathToFileEmmet = packagesPath + '/User/Emmet.sublime-settings'
		pathToUserFolder = ''
		pathToUserTemp = packagesPath + '/User/2createSaveSnippet/'
		fileToEdit = open(pathToFileEmmet, 'r')
		fileTemp = fileToEdit.read()
		fileToEdit.close()

		settings_path = packagesPath + '/SaveSnippet/settings.json'

		# Check the type argument
		# if the snippets should be saved
		# in normal Sublime or emmet style
		isSublimeSnippet = type == "sublime"

		# open file in new sublime window
		def openFileInWindow(pathToOpen):
			if sublime.version()[0] == '2':
				window.open_file(pathToOpen)
			else:
				window.openFile(pathToOpen)

		# create and write or only overwrite file
		def writeSnippet(pathToWrite, contentToWrite):
			if not os.path.exists(os.path.dirname(pathToWrite)):
				try:
					os.makedirs(os.path.dirname(pathToWrite))
				except OSError as exc: # Guard against race condition
					if exc.errno != errno.EEXIST:
						raise

			fileToWriteTo = open(pathToWrite, 'w')
			fileToWriteTo.write(contentToWrite)
			fileToWriteTo.close()

		# save data to the snippets' file
		def saveToOriginalJson(json_to_save):
			with open(pathToFile, 'w') as json_target:
				json.dump(json_to_save, json_target, indent=4)

		# Define a function for input data collecting
		# and edit target file as json
		def on_done(name):
			new_snippet = view.substr(region).replace('$', '\$')
			with open(pathToFile) as json_data:
				snippets_json = json.load(json_data)
				condition = True

				# check the file type of the current working file
				fileType = view.file_name().split('.')[-1]

				# If the file is .css/.less/.sass
				if fileType == 'css' or fileType == 'less' or fileType == 'sass':
					# if isSublimeSnippet is true it will create
					# a normal Sublime snippet not emmet
					if isSublimeSnippet:
						pathToUserFolder = pathToUserTemp +  name + '-css.sublime-snippet'

						cssSnippetFull = '<snippet>\n\t<content><![CDATA[' + new_snippet + ']]></content>\n\t<tabTrigger>' + name + '</tabTrigger>\n\t<scope>source.css</scope>\n\t<description></description>\n</snippet>'

						if os.path.exists(pathToUserFolder):
							condition = True if sublime.ok_cancel_dialog('The snippet exists! Do you want to overwrite it?') == True else False

							if condition:
								writeSnippet(pathToUserFolder, cssSnippetFull)

								openFileInWindow(pathToUserFolder)
						else:
							writeSnippet(pathToUserFolder, cssSnippetFull)

							openFileInWindow(pathToUserFolder)
					else:
						if name in snippets_json['css']['snippets'] or fileTemp.find(name) != -1:
							condition = True if sublime.ok_cancel_dialog('The snippet exists! Do you want to overwrite it?') == True else False
						if condition:
							# Edit original snippets file /User/Emmet.sublime-settings
							# by adding 'Overwritten' at the end of the original snippet name
							snippetOverwritten = fileTemp.replace('"' + name + '"', '"' + name + 'OVERWRITTEN"')
							fileToEdit = open(pathToFileEmmet, 'w')
							fileToEdit.write(snippetOverwritten)
							fileToEdit.close()
							# replace the value of the existing key in the plug-in's shippets file
							snippets_json['css']['snippets'].update({name: new_snippet})
							saveToOriginalJson(snippets_json)

				# If the file is .html
				if fileType == 'html':
					# if isSublimeSnippet is true it will create
					# a normal Sublime snippet not emmet
					if isSublimeSnippet:
						pathToUserFolder = pathToUserTemp +  name + '-html.sublime-snippet'

						htmlSnippetFull = '<snippet>\n\t<content><![CDATA[' + new_snippet + ']]></content>\n\t<tabTrigger>' + name + '</tabTrigger>\n\t<scope>text.html</scope>\n\t<description></description>\n</snippet>'

						if os.path.exists(pathToUserFolder):
							condition = True if sublime.ok_cancel_dialog('The snippet exists! Do you want to overwrite it?') == True else False

							if condition:
								writeSnippet(pathToUserFolder, htmlSnippetFull)

								openFileInWindow(pathToUserFolder)
						else:
							writeSnippet(pathToUserFolder, htmlSnippetFull)

							openFileInWindow(pathToUserFolder)
					else:
						html = new_snippet.replace('<!--', '').replace('-->', '').replace('/.', '').replace('\n', '').replace('\t', '')
						htmlTemp = ''
						htmlRole = 'main'
						htmlAbbr = ''
						htmlAllowOldTag = True
						htmlAllowNewTag = False
						htmlOldTag = ''
						htmlNewTag = ''
						copyState = True

						if name in snippets_json['html']['abbreviations']:
							condition = True if sublime.ok_cancel_dialog('The abbreviation exists! Do you want to overwrite it?') == True else False
						if condition:
							htmlTemp = html

							# strip htmlTemp from the plain text
							for letter in htmlTemp:
								if letter == '<':
									copyState = True
								if copyState:
									htmlAbbr += letter
								if letter == '>':
									copyState = False
							htmlTemp = htmlAbbr.replace('"', '').replace('</', '<z2c@b').replace('/>', 'z2c@b>').replace('/ >', 'z2c@b>')
							htmlTemp = htmlTemp.replace('/', 'o89k2')
							htmlTemp = htmlTemp.replace('z2c@b', '/')

							# wrap everything in the opening tags in "[" and "]"
							# except for the tag name
							htmlAbbr = ''
							htmlRole = 'attribute'
							for letter in htmlTemp:
								if letter == ' ':
									if htmlRole == 'attribute':
										htmlAbbr += '['
										htmlRole = 'attribute-end'
										continue
								if letter == '>' or letter == '/':
									if htmlRole == 'attribute-end':
										if letter == '>':
											htmlAbbr += ']>'
										else:
											htmlAbbr += ']/'
										htmlRole = 'attribute'
										continue
								htmlAbbr += letter

							htmlTemp = htmlAbbr.replace('<', '').replace('/>/', '^/').replace('/>', '+').replace('>/', '/')
							htmlAbbr = ''
							htmlRole = 'in'
							copyState = True

							# put ^ and + for building hierarchy
							outCounter = 0;
							for letter in htmlTemp:
								if letter == '/' and htmlRole == 'in':
									htmlRole == 'out'
								if letter == '/' and htmlRole == 'out':
									htmlRole == 'outer'

								# increase counter
								if letter == '/':
									outCounter += 1

								# forbit old/new tag copying
								if letter == '/' or letter == '[':
									htmlAllowOldTag = False
								if letter == '>':
									htmlAllowNewTag = False

								# copy old and new tags
								if htmlAllowOldTag and letter != '>':
									htmlOldTag += letter
								if htmlAllowNewTag:
									htmlNewTag += letter

								if letter == '>':
									if htmlOldTag == htmlNewTag and htmlNewTag != '':
										htmlAbbr += '+'
									else:
										htmlAbbr += '^' * (outCounter - 1)
										outCounter = 0
									htmlOldTag = ''
									htmlNewTag = ''

								# allow old/new tag copying
								if letter == '>':
									htmlAllowOldTag = True
								if letter == '/':
									copyState = False
									htmlAllowNewTag = True

								if copyState:
									htmlAbbr += letter
								if letter == '>':
									copyState = True

							htmlAbbr = htmlAbbr.replace('o89k2', '/').replace(': ', ':').replace('; ', ';')
							htmlAbbr = htmlAbbr.replace('nav[', 'div[')
							snippets_json['html']['abbreviations'].update({name: htmlAbbr})
							saveToOriginalJson(snippets_json)

				# If the file is .js
				if fileType == 'js':
					pathToUserFolder = pathToUserTemp +  name + '-js.sublime-snippet'

					jsSnippetFull = '<snippet>\n\t<content><![CDATA[' + new_snippet + ']]></content>\n\t<tabTrigger>' + name + '</tabTrigger>\n\t<scope>source.js</scope>\n\t<description></description>\n</snippet>'

					if os.path.exists(pathToUserFolder):
						condition = True if sublime.ok_cancel_dialog('The snippet exists! Do you want to overwrite it?') == True else False

						if condition:
							writeSnippet(pathToUserFolder, jsSnippetFull)

							openFileInWindow(pathToUserFolder)
					else:
						writeSnippet(pathToUserFolder, jsSnippetFull)

						openFileInWindow(pathToUserFolder)

		# check if snippet file exists and if not,
		# create it and add initial content in it
		if not os.path.exists(pathToFile):
			open(pathToFile, 'w')
			initial_json = {"css":{"snippets":{}},"html":{"abbreviations":{}}}
			saveToOriginalJson(initial_json)

		# Take the selected text and
		# open window to name the snippet
		for region in regions:
			region = view.word(region)
			window = sublime.active_window()
			window.show_input_panel('Write snippet name:', '', on_done, None, None)

